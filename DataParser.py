import csv
from HashMap import HashMap

# This file parses the data from CSV files made from the Excel files provided

with open('WGUPSpackages.csv') as packageData:
    tempCSV = csv.reader(packageData, delimiter=',')
    packagesHashMap = HashMap() # Uses the HashMap constructor to make a hash table

    # Space-time complexity: O(N)
    for record in tempCSV:
        note = ''
        if len(record) == 8:
            note = record[7]
        else:
            note = 'NONE'
        if 'Wrong' in note:
            record[1] = '410 S State St'
            record[4] = '84111'
        tempList = [record[0], record[1], record[2], record[3],
                     record[4], record[5], record[6], note, 'AT_HUB', 0, 0]
        # The second to last index is the delivery timestamp, and the last index is the truck it is on. Default is 0
        packagesHashMap.insertIntoMap(record[0], tempList)  # inserts each parsed package into the hash table

with open('DistanceData.csv') as distanceDataCsv:
    distanceData = list(csv.reader(distanceDataCsv, delimiter=','))
# I modified the CSV file to include the address in the first index of every record

# Returns the hash table of packages
# Space-time complexity: O(1)
def getHashMap():
    return packagesHashMap

# Returns a package from the hash table
# Space-time complexity: O(1)
def getPackage(key):
    return packagesHashMap.getPackage(key)
