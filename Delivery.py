from DataParser import packagesHashMap, distanceData
import datetime

# The 'startTruckProcess' function takes the packages, truck number and the departing time
# the packages are organized based on distance and package note conditionals and then begins delivering them out
# this function calls the nearest neighbor algorithm
# Space-time complexity: O(N^3)
def startTruckProcess(truckNum, departureTime):
    currentLoc = '4001 South 700 East' # starting location
    returnToHub = [['0', '4001 South 700 East', 'Salt Lake City', 'UT', '84104', 'EOD', '0', 'NONE', 'AT_HUB']]
    packageIDs = [[15,1,13,14,16,20,29,30,31,34,37,40,19,4,23,39],
                  [6, 25, 3, 8, 18, 28, 32, 33, 36, 38, 10, 11, 12, 17, 7, 5],
                  [2,9,21,22,24,26,27,35]] # holds which package goes on which truck respectively

    truckDeliveryRoute = []  # contains the ordered delivery route of the packages
    previousIndex = 1  # starting index

    if truckNum == 1:
        filteredPackages = getPackages(packageIDs[0], list(packagesHashMap.getAllPackages()))
    if truckNum == 2:
        filteredPackages = getPackages(packageIDs[1], list(packagesHashMap.getAllPackages()))
    if truckNum == 3:
        filteredPackages = getPackages(packageIDs[2], list(packagesHashMap.getAllPackages()))

    # Space-time complexity: O(N^3)
    for count in range(len(filteredPackages)):
        # for each of the trucks packages it will loop through finding the most efficient route

        truckDeliveryRoute.append(distanceCheck(previousIndex, filteredPackages, truckNum))
        currentLoc = truckDeliveryRoute[-1][1] # the next location to go to in the delivery route
        previousIndex = getLocationIndex(currentLoc)  # obtain the next destination index

        for p in filteredPackages:
            if p[0] == truckDeliveryRoute[-1][0]: # last appended delivery route and remove from filteredPackages
                filteredPackages.remove(p)
                break

        if len(truckDeliveryRoute) == 16: # if truck is full then remove packages from left over packages
            if truckNum == 1: # Truck 1 must return to drive truck 3, though 2 or 3 do not need to return
                truckDeliveryRoute.append(distanceCheck(previousIndex, returnToHub, truckNum))

    # delivery result contains results from the deliver function
    deliveryResult = deliver(truckDeliveryRoute, departureTime, truckNum)
    return deliveryResult

# Returns the packages with the given IDs
# Space-time complexity: O(N^2)
def getPackages(staticList, packages):
    packageRetrieved = []
    for staticPackageKey in staticList:
        staticPackage = packagesHashMap.getPackage(staticPackageKey)
        for package in packages:
            if staticPackage == package:
                packageRetrieved.append(package)
    return packageRetrieved

# The deliver function takes the route miles, and makes a timestamp in the hashtable when it is delivered
# Space-time complexity: O(N)
def deliver(truckPackages, departure, truckNum):
    totalMiles = 0
    totalTime = datetime.timedelta(hours=0, minutes=0, seconds=0)
    (hours, minutes, seconds) = departure.split(':')
    time = datetime.timedelta(hours=int(hours), minutes=int(minutes), seconds=int(seconds))
    # time = departure
    for package in truckPackages:
        totalMiles += float(package[2])
        deliveredTimestamp = totalMiles / 18
        deliveredTimestamp = datetime.timedelta(hours=int(deliveredTimestamp),
                                                minutes=(deliveredTimestamp*60) % 60,
                                                seconds=(deliveredTimestamp*3600) % 60)
        deliveredTimestamp = time + deliveredTimestamp
        totalTime = time + totalTime
        up = packagesHashMap.getPackage(package[0])

        if up is not None:
            up = [up[0], up[1], up[2], up[3], up[4], up[5], up[6], up[7], 'DELIVERED', deliveredTimestamp, truckNum]
            packagesHashMap.update(package[0], up)

    return [totalTime, totalMiles]

def distanceCheck(currentLocIndex, filteredPackages, truckNum):
    distanceList = []

    for package in filteredPackages:
        for location in distanceData:
            if package[1] == location[0]:
                if location[currentLocIndex] != '':
                    distanceList.append([package[0], location[0], float(location[currentLocIndex])])
                else:
                    distanceList.append([package[0], location[0], float(distanceData[currentLocIndex - 1][currentLocIndex - 1])])

    distanceList.sort(key=lambda x: x[2])
    return distanceList[0]


def getLocationIndex(address):
    count = 0
    for location in distanceData:
        count += 1
        if location[0] == address:
            return count