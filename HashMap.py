

class HashMap:
    # Constructor
    def __init__(self):
        # Creates an empty bucket list
        self.map = []
        for i in range(10):
            self.map.append([])

    # Generates a hash key
    # Space-time complexity: O(1)
    def getHash(self, key):
        bucket = int(key) % len(self.map)
        return bucket

    # Inserts a package into the hash table
    # Space-time complexity: O(N)
    def insertIntoMap(self, key, value):
        keyHash = self.getHash(key)
        keyVal = [key, value]

        if len(self.map[keyHash]) == 0:
            self.map[keyHash] = list([keyVal])
            return True
        else:
            for pair in self.map[keyHash]:
                if pair[0] == key:
                    pair[1] = keyVal
                    return True
                self.map[keyHash].append(keyVal)
                return True

    # get a single package from the has table
    # O(N)
    def getPackage(self, key):
        keyHash = self.getHash(key)
        if self.map[keyHash] is not None:
            for pair in self.map[keyHash]:
                if pair[0] == str(key):
                    return pair[1]
        return None

    # getAllPackages returns all packages in the hashtable
    # O(N)
    def getAllPackages(self):
        allPackages = []

        for obj in self.map:
            for seco in obj:
                allPackages.append(seco[1])
        return allPackages

    # Update a package from the hash table
    # O(N)
    def update(self, key, value):
        # print('update')
        key_hash = self.getHash(key)
        if self.map[key_hash] is not None:
            for pair in self.map[key_hash]:
                if pair[0] == key:
                    pair[1] = value
                    # print(pair[1])
                    return True
        else:
            print('There was an error with updating on key: ' + key)

    # Deletes a package from the hash table
    # O(N)
    def deletePackage(self, key):
        keyHash = self.getHash(key)
        if self.map[keyHash] is None:
            return False
        for i in range(0, len(self.map[keyHash])):
            if self.map[keyHash][i][0] == key:
                self.map[keyHash].pop(i)
                return True