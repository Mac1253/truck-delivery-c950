from DataParser import  getPackage, getHashMap, packagesHashMap
from Delivery import startTruckProcess
import datetime
import sys

# Author: Michael Corrigan
# Student ID: 001255411

commandList = 'List of commands\n\tlookup\n\ttimestamp\n\texit\n'

# The main function, starts the delivery process, and asks for user input
def main():
    exit = False
    datetime.timedelta(hours=8, minutes=0, seconds=0)
    truckDepatures = [datetime.timedelta(hours=8, minutes=0, seconds=0),
                      datetime.timedelta(hours=9, minutes=5, seconds=0),
                      datetime.timedelta(hours=10, minutes=58, seconds=0)] # truck departure times in timedelta format
    totalMiles = 0
    routeFinishTime = datetime.timedelta(days=0, hours=0, minutes=0, seconds=0)

    # This loops 3 times, for each truck to organize and then deliver
    # O(N)
    for departure in range(1, 4):
        results = startTruckProcess(departure, str(truckDepatures[departure - 1]))
        totalMiles += results[1]
        routeFinishTime += results[0]
    print('All delivery routes have finished totaling', totalMiles,)



    while exit is False:
        command = input('\n' + commandList)
        if command == 'lookup':
            try:
                packageID = input('Enter a package ID: ')

                if int(packageID) <= 0  or int(packageID) > 40:
                    print('ERROR: Please enter a valid number 1 through 40')
                    break

                time = input('At what time? 24 hour clock Format in hh:mm:ss ')
                (hours, minutes, seconds) = time.split(':')
                time = datetime.timedelta(hours=int(hours), minutes=int(minutes), seconds=int(seconds))
                package = getPackage(packageID)
                status = ''
                timestampStr = '\tEstimated time of arrival: ' + str(package[9])
                if package[9] < time:
                    status = 'Delivered'
                    timestampStr = '\tDelivered timestamp: ' + str(package[9])
                elif package[9] > time < truckDepatures[package[10] - 1]:
                    status = 'At Hub'
                else:
                    status = 'En route'
                print('ID:', package[0], '\tAddress:', package[1], ',', package[2], ',', package[3], ',', package[4],
                      '\tDeadline:', package[5], '\tWeight:', package[6], '\tStatus:', status, timestampStr, 'Truck#:', package[10])
            except ValueError:
                print('Invalid input, please try again')
                pass

        elif command == 'timestamp':
            print('A list of all packages and their status')
            try:
                time = input('At what time? 24 hour clock Format in hh:mm:ss ')
                (hours, minutes, seconds) = time.split(':')
                time = datetime.timedelta(hours=int(hours), minutes=int(minutes), seconds=int(seconds))
                packages = packagesHashMap.getAllPackages()
                packages.sort(key=lambda x: int(x[0]))
                status = ''

                for package in packages:
                    timestampStr = '\tEstimated time of arrival: ' + str(package[9])
                    if package[9] < time:
                        status = 'Delivered'
                        timestampStr = '\tDelivered timestamp: ' + str(package[9])
                    elif package[9] > time < truckDepatures[package[10] - 1]:
                        status = 'At Hub'
                    else:
                        status = 'En route'
                    print('ID:', package[0], '\tAddress:', package[1], ',', package[2], ',', package[3], ',', package[4],
                          '\tDeadline:', package[5], '\tWeight:', package[6], '\tStatus:', status, timestampStr, 'Truck#:', package[10])
            except ValueError:
                print('Invalid input, please try again')
                pass
        elif command == 'exit':
            sys.exit(0)
        elif command == 'pc' or command == 'pcp':
            print('A list of all packages and their status')
            time = input('At what time? 24 hour clock Format in hh:mm:ss ')
            (hours, minutes, seconds) = time.split(':')
            time = datetime.timedelta(hours=int(hours), minutes=int(minutes), seconds=int(seconds))
            packages = packagesHashMap.getAllPackages()
            packages.sort(key=lambda x: int(x[0]))
            status = ''

            for package in packages:
                timestampStr = '\tEstimated time of arrival: ' + str(package[9])
                if package[9] < time:
                    status = 'Delivered'
                    timestampStr = '\tDelivered timestamp: ' + str(package[9])
                elif package[9] > time < truckDepatures[package[10] - 1]:
                    status = 'At Hub'
                else:
                    status = 'En route'
                if package[5] != 'EOD' and package[9] > time:
                    print('ID:', package[0],
                     '\tDeadline:', package[5], '\tWeight:', package[6], '\tStatus:', status, timestampStr, 'trucknum:', package[10])
                elif package[5] != 'EOD' and command == 'pcp':
                    print('ID:', package[0],
                          '\tDeadline:', package[5], '\tWeight:', package[6], '\tStatus:', status, timestampStr,
                          'trucknum:', package[10])


        else:
            print('No such command', '\'' + command + '\'')



if __name__ == '__main__':
    print('Welcome to the Western Governors University Parcel Service\nPath finding made easy!')
    main() # Runs the main program
